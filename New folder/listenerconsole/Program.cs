﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading;
using Newtonsoft.Json;
using System.IO;

namespace listenerconsole
{
    class Program
    {

        public static string pathfilecontroller()
        {
            var contentRoot = Directory.GetCurrentDirectory() + "/" + "wwwroot";
            var logfile = "log.txt";
            var xs = Path.Combine(contentRoot, logfile);
            return xs;
        }

        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "localhost", UserName = "mqadmin", Password = "mqadminpassword" };
            string queueName = "queue22";
            var rabbitMqConnection = factory.CreateConnection();
            var rabbitMqChannel = rabbitMqConnection.CreateModel();

            rabbitMqChannel.QueueDeclare(queue: queueName,
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            rabbitMqChannel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            int messageCount = Convert.ToInt16(rabbitMqChannel.MessageCount(queueName));
            Console.WriteLine(" Di Queue ini ada {0} notification", messageCount);


            var consumer = new EventingBasicConsumer(rabbitMqChannel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
               // var data = JsonConvert.DeserializeObject<persons>(message);
               // Console.WriteLine(" Location received: " + message);
                Console.WriteLine(" [x] Diterima {0}", message);

                rabbitMqChannel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: true);
                var xs = pathfilecontroller();
                System.IO.File.AppendAllText(xs, message);
                Thread.Sleep(6000);
            };
            rabbitMqChannel.BasicConsume(queue: queueName,
                                 autoAck: true,
                                 consumer: consumer);

            Thread.Sleep(6000 * messageCount);
            Console.WriteLine(" Koneksi terputus.");
           

            Console.ReadKey();




        }
    }
}
