﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RabbitMQ;
using RabbitMQ.Client;
using rabbitmqservice.models;
namespace rabbitmqservice.logic
{
    public class logicrabbit
    {
       
        public string rabbitpesan(string nama, int umur, string pesan, string queuename)
        {
            var factory = new ConnectionFactory() { HostName = "localhost", UserName = "mqadmin", Password = "mqadminpassword" };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: queuename,
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);


                string message = "nama anda: " + nama + "umur anda " + umur + "pesan anda :" + pesan;

                //  var jsondata = JsonConvert.SerializeObject(message);

                var body = Encoding.UTF8.GetBytes(message);




                channel.BasicPublish(exchange: "",
                                     routingKey: queuename,
                                     basicProperties: null,
                                     body: body);

                return message;


            }
        }


            public bool rabbitpesan2()
            {
            try
            {
                persons personal = new persons();
                var factory = new ConnectionFactory() { HostName = "localhost", UserName = "mqadmin", Password = "mqadminpassword" };

                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: personal.queuename,
                                         durable: false,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);


                    string message = "nama anda: " + personal.nama + "umur anda " + personal.umur + "pesan anda :" + personal.pesan;

                    //  var jsondata = JsonConvert.SerializeObject(message);

                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(exchange: "",
                                          routingKey: personal.queuename,
                                          basicProperties: null,
                                          body: body);

                    if (message == null)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }

                   // return true;



                }
            }
            catch(Exception ex)
            {
                return false;
            }



            }
        }
}
