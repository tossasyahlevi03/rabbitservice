﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rabbitmqservice.models
{
    public class persons
    {
        public string nama { get; set; }
        public int umur { get; set; }
        public string pesan { get; set; }

        public string queuename { get; set; }
    }
}
