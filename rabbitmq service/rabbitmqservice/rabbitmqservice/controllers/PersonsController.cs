﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RabbitMQ;
using RabbitMQ.Client;
using rabbitmqservice.models;
using rabbitmqservice.logic;

namespace rabbitmqservice.controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class PersonsController : ControllerBase
    {
        logicrabbit logic = new logicrabbit();

        private ILOG logger;

        public PersonsController(ILOG logger)
        {
            this.logger = logger;
        }


        [ActionNameAttribute("person")]
        [HttpPost]

        public bool Post(persons personal)
        {
            var q = false;

            var message = logic.rabbitpesan(personal.nama,personal.umur,personal.pesan,personal.queuename);
            logger.Information(message);
            logger.Warning(message);
            logger.Debug(message);
            logger.Error(message);

            if (message==null)
            {
               return false;
            }
            else
            {
               return true;
            }
               

              

            }



        }
    }
